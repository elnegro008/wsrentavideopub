package renta;

import java.util.Vector;

public class Datos {
	Vector clientes;
	Vector videos;
	Vector <Renta> rentas;
	
	public Datos(){
		clientes=new Vector<Cliente>();
		videos = new Vector<Video>();	
		rentas = new Vector<Renta>();
	}
	
	public void agregarRenta(Renta v){
		rentas.addElement(v);
	}
	public int totalPorCategoria(){
		int infantil = 0;
		int adolecentes = 0;
		int adultos = 0;
		for (int i=0; i<rentas.size();i++){
			for(int j= 0; j<rentas.elementAt(i).videos.size(); j++){
				if(rentas.elementAt(i).videos.elementAt(j).tipo == "Infantil"){
					infantil+=rentas.elementAt(i).videos.elementAt(j).precio;
				}
				if(rentas.elementAt(i).videos.elementAt(j).tipo == "Adolecentes"){
					adolecentes+=rentas.elementAt(i).videos.elementAt(j).precio;
				}
				if(rentas.elementAt(i).videos.elementAt(j).tipo == "Adultos"){
					adultos+=rentas.elementAt(i).videos.elementAt(j).precio;
				}
			}
			
		}
		System.out.println("El total por categoria infantil es $" + infantil);
		System.out.println("El total por categoria adolecentes es $" + adolecentes);
		System.out.println("El total por categoria adultos es $" + adultos);
		
		return infantil;
	}
	
}
